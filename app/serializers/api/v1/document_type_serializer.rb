class Api::V1::DocumentTypeSerializer < ActiveModel::Serializer
  attributes :id, :title, :questions

  def questions
    object.questions.order(id: :asc).map do |question|
      {
        id: question.id,
        body: question.body,
        errors: question.errors,
        _destroy: question._destroy
      }
    end
  end
end
