class Api::V1::QuestionSerializer < ActiveModel::Serializer
  attributes :id, :body

  belongs_to :document_type
  has_many :answers
end
