class Api::V1::DocumentSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  
  attributes :id, :title, :document_type_id, :answers, :file_url

  belongs_to :document_type

  def file_url
    return rails_blob_path(object.file, only_path: true)
  end

  def answers
    object.answers.map do |answer|
      {
        id: answer.id,
        body: answer.body,
        question_id: answer.question_id,
        question: answer.question
      }
    end
  end

end
