import React from "react"
import Axios from 'axios';

class DocumentList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      documents: []
    }

    this.deleteDocument = this.deleteDocument.bind(this)
  }

  deleteDocument(document_id) {
    Axios.delete('/api/v1/documents/' + document_id)
      .then(response => {
        var documents = this.state.documents
        documents = documents.filter((e) => e.id !== document_id)
        this.setState({documents: documents})
      })
      .catch(error => console.log(error))
  }

  componentDidMount() {
    Axios.get('/api/v1/documents.json')
      .then(response => {
        this.setState({
          documents: response.data
        });
      })
      .catch(error => console.log(error))
  }

  render () {
    return (
      <div>
        <div className="row">
          <h2>Documents</h2>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Document Type</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {this.state.documents.map(document => {
                return(
                  <tr key={document.id}>
                    <th scope="row">{document.id}</th>
                    <td><a href={`/documents/${document.id}`}>{document.title}</a></td>
                    <td>{document.document_type.title}</td>
                    <td className="text-right">
                      <span><a href={`/documents/${document.id}`} className="badge badge-secondary">Edit</a></span>&nbsp;
                      <span><a href='javascript;' data-toggle="modal" data-target="#confirm-delete" onClick={() => { if (window.confirm('Are you sure?')) this.deleteDocument(document.id) }} className="badge badge-warning">Delete</a></span>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        <div className="row float-right">
          <a href="/documents/new" className="btn btn-primary">Add Document</a>
        </div>
      </div>
    );
  }
}

export default DocumentList
