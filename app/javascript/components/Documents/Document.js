import React from "react"
import Axios from 'axios'

class Document extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      id: '',
      title: '',
      document_type_id: '',
      file_url: '',
      errors: [],
      answers: [],
      document_types: [],
      filename: 'Choose File',
    }

    this.handleTitleChange = this.handleTitleChange.bind(this)
    this.handleDocumentTypeChange = this.handleDocumentTypeChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleAnswer = this.handleAnswer.bind(this)
    this.handleFileUploadChange = this.handleFileUploadChange.bind(this)
  }

  componentDidMount() {
    Axios.get('/api/v1/document_types.json')
      .then(response => {
        this.setState({
          document_types: response.data
        });
      })
      .catch(error => console.log(error))

    if (this.props.id) {
      Axios.get('/api/v1/documents/' + this.props.id + '.json')
        .then(response => {
          this.setState({
            id: response.data.id,
            title: response.data.title,
            document_type_id: response.data.document_type_id,
            file_url: response.data.file_url,
            errors: [],
            answers: response.data.answers
          });
        })
        .catch(error => console.log(error))
    }
  }

  handleTitleChange(event) {
    this.setState({title: event.target.value})
  }

  handleFileUploadChange(event) {
    if(event.target.files) {
      this.setState({filename: event.target.files[0].name})
    }
  }

  handleDocumentTypeChange(event) {
    this.setState({document_type_id: event.target.value})

    if(event.target.value) {
      Axios.get('/api/v1/document_types/' + event.target.value + '.json')
        .then(response => {
          var answers = []
          response.data.questions.forEach(function(question) {
            answers.push({question_id: question.id, body: '', question: question})
          });
          this.setState({answers: answers})
        })
        .catch(error => {
          this.setState({errors: error.response.data})
        })
    } else {
      this.setState({answers: []})
    }
  }

  handleAnswer(event, question) {
    var answers = this.state.answers
    var answer = answers.find((e) => e.question_id === question.id)

    if (answer) {
      answer.body = event.target.value
    } else {
      answers.push({id: '', question_id: question.id, body: event.target.value})
    }

    this.setState({answers: answers})
  }

  handleSubmit(event) {
    event.preventDefault()

    const data = new FormData();
    data.append('document[id]', this.state.id);
    data.append('document[document_type_id]', this.state.document_type_id);
    data.append('document[title]', this.state.title);
    this.state.answers.forEach(function(answer) {
      if(answer.id) {
        data.append('document[answers_attributes][][id]', answer.id);
      }
      data.append('document[answers_attributes][][body]', answer.body);
      data.append('document[answers_attributes][][question_id]', parseInt(answer.question_id));
    })

    if(this.uploadInput.files.length > 0) {
      data.append('document[file]', this.uploadInput.files[0]);
    }

    if (this.state.id) {
      Axios.put('/api/v1/documents/' + this.props.id, data)
      .then(response => {
        window.location = '/documents'
      })
      .catch(error => {
        this.setState({errors: error.response.data})
      })
    } else {
      Axios.post('/api/v1/documents', data)
      .then(response => {
        window.location = '/documents'
      })
      .catch(error => {
        this.setState({errors: error.response.data})
      })
    }
  }

  render () {
    return (
      <div className="card">
        <div className="card-header">Document</div>
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="DocumentTitle">Title</label>
              <input className="form-control" id="DocumentTitle" placeholder="Enter Title" value={this.state.title} onChange={this.handleTitleChange.bind(this)} />
              <span className="error">{this.state.errors.title}</span>
            </div>

            <div className="form-group">
              <label htmlFor="DocumentType">Document Type</label>
              <select value={this.state.document_type_id} onChange={this.handleDocumentTypeChange} className="form-control">
                <option value="">Please select a document type</option>
                {this.state.document_types.map((document_type) => {
                  return(<option value={document_type.id} key={document_type.id}>{document_type.title}</option>)
                })}
              </select>
              <span className="error">{this.state.errors.document_type}</span>
            </div>

            <div className="form-group">
              <label>File</label>
              <div className="custom-file">
                <input type="file" className="custom-file-input" ref={(ref) => { this.uploadInput = ref; }} onChange={this.handleFileUploadChange} />
                <label className="custom-file-label" htmlFor="File">{this.state.filename}</label>
                <span className="error">{this.state.errors.file}</span>
              </div>
            </div>

            {this.state.file_url &&
              <div className="form-group">
                <a className="btn btn-info" target='_blank' href={this.state.file_url}>Download File</a>
              </div>
            }

            <div>
              <span className="error">{this.state.errors.questions}</span>
              {this.state.answers.map((answer) => {
                return(
                  <div className="form-group" key={answer.question_id}>
                    <label>{answer.question.body}</label>
                    <input className="form-control" id="" value={answer.body} placeholder="Answer Question" onChange={event => this.handleAnswer(event, answer.question)} />
                  </div>
                )
              })}
            </div>
          </form>
        </div>
        <div className="card-footer">
          <div className="row">
            <div className="col-sm-6">
              <button type="submit" onClick={this.handleSubmit} className="btn btn-primary">Save Document</button>
            </div>
            <div className='col-sm-6 text-right'>
              <a href='/documents'>Cancel</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Document
