import React from "react"
import Axios from 'axios'

class DocumentType extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      id: '',
      title: '',
      errors: [],
      questions: [],
      index: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.addQuestion = this.addQuestion.bind(this)
    this.handleQuestion = this.handleQuestion.bind(this)
    this.removeQuestion = this.removeQuestion.bind(this)
  }

  componentDidMount() {
    if (this.props.id) {
      Axios.get('/api/v1/document_types/' + this.props.id + '.json')
        .then(response => {
          var questions = response.data.questions
          questions.forEach(function(question, index){
            question.index = index
          });

          this.setState({
            id: response.data.id,
            title: response.data.title,
            errors: [],
            questions: questions,
            index: questions.length
          });
        })
        .catch(error => console.log(error))
    }
  }

  handleChange(event) {
    this.setState({title: event.target.value})
  }

  addQuestion() {
    event.preventDefault()
    var questions = this.state.questions
    questions.push({body: '', index: this.state.index, id: '', _destroy: false})

    this.setState({
      questions: questions,
      index: this.state.index + 1
    })
  }

  removeQuestion(question_index) {
    event.preventDefault()

    var questions = this.state.questions
    var question = questions.find((e) => e.index === question_index)
    question._destroy = true
    this.setState({questions: questions})
  }

  handleQuestion(event, question) {
    var questions = this.state.questions

    questions.find((e) => e.index === question.index).body = event.target.value
    this.setState({questions: questions})
  }

  handleSubmit(event) {
    event.preventDefault()

    if (this.state.id) {
      Axios.put('/api/v1/document_types/' + this.props.id, { document_type: {id: this.state.id, title: this.state.title, questions_attributes: this.state.questions} })
      .then(response => {
        window.location = '/document_types'
      })
      .catch(error => {
        this.setState({errors: error.response.data})
      })
    } else {
      Axios.post('/api/v1/document_types/', { document_type: {id: this.state.id, title: this.state.title, questions_attributes: this.state.questions} })
      .then(response => {
        window.location = '/document_types'
      })
      .catch(error => {
        this.setState({errors: error.response.data})
      })
    }

  }

  render () {
    return (
      <div className="card">
        <div className="card-header">Document Type</div>
        <div className="card-body">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="DocumentTypeTitle">Title</label>
              <input className="form-control" id="DocumentTypeTitle" placeholder="Enter Title" value={this.state.title} onChange={this.handleChange.bind(this)} />
              <span className="error">{this.state.errors.title}</span>
            </div>

            <div><a onClick={this.addQuestion} className="btn btn-light">Add Question</a></div>
            <div>
              {this.state.questions.map((question) => {
                if (question._destroy === false) {
                  return(
                    <div className="form-group row" key={question.index}>
                      <label htmlFor="" className="col-sm-2 col-form-label text-right">Question</label>
                      <div className="col-sm-10">
                        <div className="input-group mb-3">
                          <input className="form-control" id="" placeholder="Enter Question" value={question.body} onChange={event => this.handleQuestion(event, question)} />
                          <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="button" onClick={() => this.removeQuestion(question.index)}>Remove</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                }
              })}
            </div>
          </form>
        </div>
        <div className="card-footer">
          <div className="row">
            <div className="col-sm-6">
              <button type="submit" onClick={this.handleSubmit} className="btn btn-primary">Save Document Type</button>
            </div>
            <div className='col-sm-6 text-right'>
              <a href='/document_types'>Cancel</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DocumentType
