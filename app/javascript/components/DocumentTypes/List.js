import React from "react"
import Axios from 'axios';

class DocumentTypeList extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      document_types: []
    }

    this.deleteDocumentType = this.deleteDocumentType.bind(this)
  }

  deleteDocumentType(document_type_id) {
    Axios.delete('/api/v1/document_types/' + document_type_id)
      .then(response => {
        var document_types = this.state.document_types
        document_types = document_types.filter((e) => e.id !== document_type_id)
        this.setState({document_types: document_types})
      })
      .catch(error => console.log(error))
  }

  componentDidMount() {
    Axios.get('/api/v1/document_types.json')
      .then(response => {
        this.setState({
          document_types: response.data
        });
      })
      .catch(error => console.log(error))
  }

  render () {
    return (
      <div>
        <div className="row">
          <h2>Document Types</h2>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {this.state.document_types.map(document_type => {
                return(
                  <tr key={document_type.id}>
                    <th scope="row">{document_type.id}</th>
                    <td><a href={`/document_types/${document_type.id}`}>{document_type.title}</a></td>
                    <td className="text-right">
                      <span><a href={`/document_types/${document_type.id}`} className="badge badge-secondary">Edit</a></span>&nbsp;
                      <span><a href='javascript;' data-toggle="modal" data-target="#confirm-delete" onClick={() => { if (window.confirm('Are you sure?')) this.deleteDocumentType(document_type.id) }} className="badge badge-warning">Delete</a></span>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
        <div className="row float-right">
          <a href="/document_types/new" className="btn btn-primary">Add Document Type</a>
        </div>
      </div>
    );
  }
}

export default DocumentTypeList
