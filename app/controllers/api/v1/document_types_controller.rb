class Api::V1::DocumentTypesController < ApplicationController
  before_action :find_document_type, only: [:show, :update, :destroy]

  # GET /document_types
  def index
    @document_types = DocumentType.order(created_at: :desc)

    render json: @document_types
  end

  # GET /document_types/1
  def show
    render json: @document_type
  end

  # POST /document_types
  def create
    @document_type = DocumentType.new(document_type_params)

    if @document_type.save
      render json: @document_type, status: :created
    else
      render json: @document_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH /document_type/1
  def update
    if @document_type.update(document_type_params)
      render json: @document_type
    else
      render json: @document_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /document_types/1
  def destroy
    if @document_type.destroy
      head :no_content, status: :ok
    else
      render json: @document_type.errors, status: :unprocessable_entity
    end
  end

  private

  def find_document_type
    @document_type = DocumentType.find(params[:id])
  end

  def document_type_params
    params.require(:document_type).permit(:title, questions_attributes: [:id, :body, :_destroy])
  end

end
