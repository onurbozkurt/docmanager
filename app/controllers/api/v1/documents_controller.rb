class Api::V1::DocumentsController < ApplicationController
  before_action :find_document, only: [:show, :update, :destroy]

  # GET /documents
  def index
    @documents = Document.order(created_at: :desc)

    render json: @documents
  end

  # GET /documents/1
  def show
    render json: @document
  end

  # POST /documents
  def create
    @document = Document.new(document_params)

    if @document.save
      render json: @document, status: :created
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  # PATCH /document/1
  def update
    if @document.update(document_params)
      render json: @document
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  # DELETE /document/1
  def destroy
    if @document.destroy
      head :no_content, status: :ok
    else
      render json: @document.errors, status: :unprocessable_entity
    end
  end

  private

  def find_document
    @document = Document.find(params[:id])
  end

  def document_params
    params.require(:document).permit(:id, :title, :document_type_id, :file, answers_attributes: [:id, :question_id, :body])
  end

end
