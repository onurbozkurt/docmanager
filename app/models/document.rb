class Document < ApplicationRecord

  # Relations
  belongs_to :document_type

  has_many :questions, through: :document_type
  has_many :answers, dependent: :destroy

  has_one_attached :file

  accepts_nested_attributes_for :answers, allow_destroy: true, :reject_if => lambda { |a| a[:body].blank? }

  # Validations
  validates :title, presence: true, uniqueness: true
  validate :has_file?
  validate :answered?

  private

  def has_file?
    errors.add(:file, :blank) unless file.attached?
  end

  def answered?
    if document_type
      errors.add(:questions, 'You should answer all questions') \
        unless answers.map(&:question_id) == document_type.questions.map(&:id)
    end
  end

end
