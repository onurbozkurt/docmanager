class DocumentType < ApplicationRecord

  # Relations
  has_many :questions, dependent: :destroy
  has_many :documents, dependent: :destroy
  accepts_nested_attributes_for :questions, allow_destroy: true, :reject_if => lambda { |a| a[:body].blank? }

  # Validations
  validates :title, presence: true, uniqueness: true

end
