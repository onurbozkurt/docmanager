class Answer < ApplicationRecord

  # Relations
  belongs_to :document
  belongs_to :question

  # Validations
  validates :body, presence: true

end
