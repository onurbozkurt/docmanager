class Question < ApplicationRecord

  # Relations
  belongs_to :document_type

  # Validations
  validates :body, presence: true

end
