require 'rails_helper'

RSpec.describe DocumentType, type: :model do

  context 'relations' do
    it { is_expected.to have_many(:questions).dependent(:destroy) }
    it { is_expected.to have_many(:documents).dependent(:destroy) }
    it { is_expected.to accept_nested_attributes_for(:questions).allow_destroy(true) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_uniqueness_of(:title) }
  end

end
