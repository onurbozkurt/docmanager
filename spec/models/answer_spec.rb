require 'rails_helper'

RSpec.describe Answer, type: :model do
  context 'relations' do
    it { is_expected.to belong_to(:document) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:body) }
  end
end
