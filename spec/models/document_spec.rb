require 'rails_helper'

RSpec.describe Document, type: :model do

  context 'relations' do
    it { is_expected.to belong_to(:document_type) }
    it { is_expected.to have_many(:answers).dependent(:destroy) }
    it { is_expected.to have_many(:questions).through(:document_type) }
    it { is_expected.to accept_nested_attributes_for(:answers).allow_destroy(true) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_uniqueness_of(:title) }

    context 'should validate if all questions are answered' do
      before do
        document_type = create(:document_type)
        create(:question, document_type: document_type)
        @document = build(:document, :with_file, document_type: document_type)
        # @document.file.attach(io: File.open(Rails.root.join('spec', 'factories', 'documents', 'document.txt')), filename: 'document.txt', content_type: 'text/html')
      end

      it 'returns false with invalid data' do
        expect(@document.save).to be_falsey
      end

      it 'returns true with valid data' do
        @document.answers << build(:answer, body: 'Answer', question_id: @document.document_type.questions.first.id)
        expect(@document.save).to be_truthy
      end
    end
  end

end
