require 'rails_helper'

describe "DocumentType API" do

  context "GET /document_types" do
    it 'lists document_types' do
      create_list(:document_type, 5)

      get '/api/v1/document_types'

      expect(response).to be_successful
      expect(json.length).to eq(5)
    end
  end

  context "GET /document_types/:id" do
    it 'returns document_type' do
      document_type = create(:document_type)
      get "/api/v1/document_types/#{document_type.id}"

      expect(json).not_to be_empty
      expect(json['id']).to eq(document_type.id)
      expect(response).to have_http_status(200)
    end
  end

  context "POST /document_types" do
    it 'should create document_type with valid data' do
      document_type_attributes = attributes_for(:document_type)

      expect do
        post '/api/v1/document_types', params: {document_type: document_type_attributes}
      end.to change { DocumentType.count }.by(1)

      expect(json).not_to be_empty
      expect(json['title']).to eq(document_type_attributes[:title])
      expect(response).to have_http_status(201)
    end

    it 'should create document_type with questions' do
      question1 = attributes_for(:question)
      question2 = attributes_for(:question)
      document_type_attributes = attributes_for(:document_type, questions_attributes: [question1, question2])

      expect do
        post '/api/v1/document_types', params: {document_type: document_type_attributes}
      end.to change { Question.count }.by(2)
    end

    it 'should returns error if validation fails' do
      expect do
        post '/api/v1/document_types', params: {document_type: {title: ''}}
      end.to change { DocumentType.count }.by(0)

      expect(response.body).to match(/can't be blank/)
      expect(response).to have_http_status(422)
    end
  end

  describe 'PATCH /document_types/:id' do
    it 'should update document_type with valid data' do
      document_type = create(:document_type)

      expect do
        patch "/api/v1/document_types/#{document_type.id}", params: {document_type: {title: 'Updated Title'}}
      end.to change { DocumentType.count }.by(0)

      expect(json).not_to be_empty
      expect(json['title']).to eq('Updated Title')
      expect(response).to have_http_status(200)
    end

    it 'should returns error if validation fails' do
      document_type = create(:document_type)

      expect do
        patch "/api/v1/document_types/#{document_type.id}", params: {document_type: {title: ''}}
      end.to change { DocumentType.count }.by(0)

      expect(response.body).to match(/can't be blank/)
      expect(response).to have_http_status(422)
    end
  end

  describe 'DELETE /document_types/:id' do
    it 'should destroy document_type' do
      document_type = create(:document_type)

      expect do
        delete "/api/v1/document_types/#{document_type.id}"
      end.to change { DocumentType.count }.by(-1)

      expect(response).to have_http_status(204)
    end
  end

end
