require 'rails_helper'

describe "Document API" do

  context "GET /documents" do
    it 'lists documents' do
      create_list(:document, 5, :with_file)

      get '/api/v1/documents'

      expect(response).to be_successful
      expect(json.length).to eq(5)
    end
  end

  context "GET /documents/:id" do
    it 'returns document' do
      document = create(:document, :with_file)
      get "/api/v1/documents/#{document.id}"

      expect(json).not_to be_empty
      expect(json['id']).to eq(document.id)
      expect(response).to have_http_status(200)
    end
  end

  context "POST /documents" do
    it 'should create document with valid data' do
      document_type = create(:document_type)
      document_attributes = attributes_for(:document, :with_file, document_type_id: document_type.id)

      expect do
        post '/api/v1/documents', params: {document: document_attributes}
      end.to change { Document.count }.by(1)

      expect(json).not_to be_empty
      expect(json['title']).to eq(document_attributes[:title])
      expect(response).to have_http_status(201)
    end

    it 'should create document with answers' do
      document_type = create(:document_type)
      question = create(:question, document_type: document_type)
      answer = attributes_for(:answer, question_id: question.id)

      document_attributes = attributes_for(:document, :with_file, document_type_id: document_type.id, answers_attributes: [answer])

      expect do
        post '/api/v1/documents', params: {document: document_attributes}
      end.to change { Answer.count }.by(1)
    end

    it 'should returns error if validation fails' do
      expect do
        post '/api/v1/documents', params: {document: {title: ''}}
      end.to change { Document.count }.by(0)

      expect(response.body).to match(/can't be blank/)
      expect(response).to have_http_status(422)
    end
  end

  describe 'PATCH /documents/:id' do
    it 'should update document with valid data' do
      document = create(:document, :with_file)

      expect do
        patch "/api/v1/documents/#{document.id}", params: {document: {title: 'Updated Title'}}
      end.to change { Document.count }.by(0)

      expect(json).not_to be_empty
      expect(json['title']).to eq('Updated Title')
      expect(response).to have_http_status(200)
    end

    it 'should returns error if validation fails' do
      document = create(:document, :with_file)

      expect do
        patch "/api/v1/documents/#{document.id}", params: {document: {title: ''}}
      end.to change { Document.count }.by(0)

      expect(response.body).to match(/can't be blank/)
      expect(response).to have_http_status(422)
    end
  end

  describe 'DELETE /documents/:id' do
    it 'should destroy document' do
      document = create(:document, :with_file)

      expect do
        delete "/api/v1/documents/#{document.id}"
      end.to change { Document.count }.by(-1)

      expect(response).to have_http_status(204)
    end
  end

end
