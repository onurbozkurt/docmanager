FactoryBot.define do
  factory :document_type do
    title { Faker::Name.name }
  end
end
