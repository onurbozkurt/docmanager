FactoryBot.define do
  factory :document do
    document_type { create(:document_type) }
    title { Faker::Book.title }

    trait :with_file do
      file { fixture_file_upload(Rails.root.join('spec', 'factories', 'documents', 'document.txt'), 'text/html') }
    end
  end
end
