# DOC Manager

A small Ruby on Rails & ReactJS application to manage documents

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

```
Ruby 2.5.0
Rails 5.2.1
Node v8.12.0 or greater
```

### Installing

Install the bundler gem

```
gem install bundler
```

Install project dependencies

```
bundle install
yarn install
```

Setup Database

```
rails db:create
rails db:migrate
```

Create .env and update with S3 Credentials (Required for production)

```
cp .env.example .env
```

## Running the application

You can run the application with following command

```
rails server
```

## Running the tests

The projects uses RSpec for testing. To run all the tests, run the following command.

```
bundle exec rspec
```

## Missing features

* Refactor react components
* Write more tests, especially for views
* Add paging support for lists
* Add search & filter support for documents