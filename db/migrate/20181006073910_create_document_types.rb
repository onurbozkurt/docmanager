class CreateDocumentTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :document_types do |t|
      t.string :title, index: true, unique: true

      t.timestamps
    end
  end
end
