class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.references :document_type
      t.text :body

      t.timestamps
    end
  end
end
