# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

10.times do
  document_type = FactoryBot.create(:document_type)
  FactoryBot.create_list(:question, 7, document_type: document_type)
end
