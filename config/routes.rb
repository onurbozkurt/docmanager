Rails.application.routes.draw do

  root to: 'home#index'

  # Frontend
  resources :document_types, only: [:index, :new, :show]
  resources :documents, only: [:index, :new, :show]

  # API
  namespace :api do
    namespace :v1 do
      resources :document_types do
        resources :questions
      end

      resources :documents do
        resources :answers
      end
    end
  end

end
